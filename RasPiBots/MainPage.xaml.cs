﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Telegram.Bot;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Telegram.Bot.Types;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using HtmlAgilityPack;
using Windows.ApplicationModel;

// Die Vorlage "Leere Seite" ist unter http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409 dokumentiert.

namespace RasPiBots
{
    /// <summary>
    /// Eine leere Seite, die eigenständig verwendet oder zu der innerhalb eines Rahmens navigiert werden kann.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage() 
        {
            TelegramBotClient mittag = new TelegramBotClient("214403515:AAEcSskPjloh4VVWCUhZ3Asr_xaoE9NpJio");
            TelegramBotClient hure = new TelegramBotClient("271200674:AAEtDPvNJ4bBcZqljqE4_gSQQ4SVNt0ahgE");
            MittagBot mBot = new MittagBot(mittag);
            HurenBot hBot = new HurenBot(hure);

            
            mittag.OnMessage += (sender, e) => mBot.NachrichtErhalten(e.Message);
            mittag.StartReceiving();
            
            this.InitializeComponent();
        }
                

    }
}
