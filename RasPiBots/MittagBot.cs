﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;
using Windows.ApplicationModel;
using Windows.UI.Xaml;

namespace RasPiBots
{

    class MittagBot
    {
        public DateTime StartDate { get; set; }
        public long BotNet { get; set; }
        public TelegramBotClient Mittag { get; set; }
        public DispatcherTimer Gear { get; set; }
        public DispatcherTimer MittagTimer { get; set; }
        public long ChatID { get; set; }
        public long SetoKaibaMessageID { get; set; }
        public DateTime OgTime { get; set; }
        public bool IsOgTime { get; set; }
        public bool IsGeplant { get; set; }
        public string Geplant { get; set; }
        public long GeplantMessageID { get; set; }


        public MittagBot(TelegramBotClient api)
        {
            try
            {
                BotNet = -107577394;
                Mittag = api;
                Gear = new DispatcherTimer();
                Gear.Interval = new TimeSpan(0, 0, (60 - DateTime.Now.Second));
                Gear.Tick += Gear_Tick;
                Gear.Start();

                Mittag.GetUpdatesAsync();

                IsOgTime = false;
                IsGeplant = false;

                MittagTimer = new DispatcherTimer();
                MittagTimer.Interval = new TimeSpan(0, 1, 0);
                MittagTimer.Tick += MittagTimer_Tick;
                StartDate = DateTime.Now;

                Package pack = Package.Current;
                PackageId packID = pack.Id;
                PackageVersion packVers = packID.Version;

                //Mittag.SendTextMessageAsync(BotNet, $"MittagsBot {packVers.Major}.{packVers.Minor}.{packVers.Build}.{packVers.Revision} just Started!");

            }
            catch (Exception e)
            {
                SendException(e);
            }
        }

        private void Gear_Tick(object sender, object e)
        {
            MittagTimer.Start();
            Gear.Stop();
        }

        private async void MittagTimer_Tick(object sender, object e)
        {
            try
            {
                Random rand = new Random();

                FeiertagLogic f = FeiertagLogic.GetInstance(DateTime.Today.Year);
                bool feiertag = f.FesteFeiertage.Where(x => x.Datum == DateTime.Today).Any();

                if (!feiertag && !(DateTime.Today.DayOfWeek == DayOfWeek.Saturday) && !(DateTime.Today.DayOfWeek == DayOfWeek.Sunday))
                {
                    if (DateTime.Now.Hour == 10 && DateTime.Now.Minute == 00 && !IsGeplant)
                        await Mittag.SendTextMessageAsync(ChatID, "Was geht Mittag?");
                    if (IsOgTime && DateTime.Now.Hour == OgTime.Hour && DateTime.Now.Minute == OgTime.Minute)
                    {
                        await Mittag.SendTextMessageAsync(ChatID, $"{String.Concat(Enumerable.Repeat("og", rand.Next(1, 10)))} \n{Geplant}");
                        IsOgTime = false;
                        IsGeplant = false;
                        Geplant = null;
                    }
                    if (DateTime.Now.Hour == 17 && DateTime.Now.Minute == 00)
                        await Mittag.SendTextMessageAsync(ChatID, "Feierabend!");
                }
            }
            catch (Exception exp)
            {
                SendException(exp);
            }

        }

        public async void NachrichtErhalten(Message nachricht)
        {
            try
            {
                string[] command = new string[] { @"/setog[.\S]* *(.*)", @"/geplant[.\S]* *(.*)", @"/gettime", @"/subdestages", @"/jerrydestages" };

                if (nachricht.Type == MessageType.TextMessage)
                {

                    #region setog
                    if (Regex.IsMatch(nachricht.Text, command[0]))
                    {
                        MatchCollection zeit = Regex.Matches(nachricht.Text, command[0]);
                        DateTime ogTime;
                        IsOgTime = ZeitStringZuDatum(zeit[0].Groups[1].Value, out ogTime);

                        if (IsOgTime)
                        {
                            OgTime = ogTime;
                            await Mittag.SendTextMessageAsync(nachricht.Chat.Id, $"Alles klar, ogog um {OgTime.ToString("HH:mm")}", replyToMessageId:nachricht.MessageId);
                        }
                        else
                        {
                            ReplyKeyboardMarkup rkm = new ReplyKeyboardMarkup();
                            rkm.Keyboard = new KeyboardButton[][]
                            {
                            new KeyboardButton[] { "11:00", "11:30" },
                            new KeyboardButton[] { "12:00", "12:30" },
                            new KeyboardButton[] { "13:00", "13:30" },
                            new KeyboardButton[] { "14:00", "Kein ogog"}
                            };
                            rkm.OneTimeKeyboard = true;
                            rkm.Selective = true;

                            Message gesendeteNachricht = await Mittag.SendTextMessageAsync(nachricht.Chat.Id, "Um wie viel Uhr ist Og?", replyToMessageId:nachricht.MessageId, replyMarkup: rkm);
                            SetoKaibaMessageID = gesendeteNachricht.MessageId;
                        }
                    }
                    #endregion
                    #region Geplant
                    if (Regex.IsMatch(nachricht.Text, command[1]))
                    {

                        MatchCollection planung = Regex.Matches(nachricht.Text, command[1]);
                        Geplant = planung[0].Groups[1].Value;
                        if (string.IsNullOrEmpty(Geplant))
                        {
                            IsGeplant = false;
                            ReplyKeyboardMarkup rkm = new ReplyKeyboardMarkup();
                            rkm.Keyboard = new KeyboardButton[][]
                                {
                                    new KeyboardButton[] { "Netto", "Edeka", "Kaufland" },
                                    new KeyboardButton[] { "Döner", "Asia", "Subway"},
                                    new KeyboardButton[] { "Sandwich", "BK", "Food Rockers"}
                                };
                            rkm.OneTimeKeyboard = true;
                            rkm.Selective = true;

                            Message gesendeteNachricht = await Mittag.SendTextMessageAsync(nachricht.Chat.Id, "Was ist geplant?", replyToMessageId: nachricht.MessageId, replyMarkup:rkm);
                            GeplantMessageID = gesendeteNachricht.MessageId;
                        }
                        else
                        {
                            IsGeplant = true;
                            Message gesendeteNachricht = await Mittag.SendTextMessageAsync(nachricht.Chat.Id, $"{Geplant} ist geplant");
                        }
                    }
                    #endregion
                    #region gettime
                    if (Regex.IsMatch(nachricht.Text, command[2]))
                    {
                        await Mittag.SendTextMessageAsync(nachricht.Chat.Id, DateTime.Now.ToString("HH:mm"));
                    }
                    #endregion
                    #region subdestages
                    if (Regex.IsMatch(nachricht.Text, command[3]) || Regex.IsMatch(nachricht.Text, command[4]))
                    {
                        try
                        {
                            await Mittag.SendTextMessageAsync(nachricht.Chat.Id, await getSubDesTages());
                        }
                        catch (Exception e)
                        {
                            await Mittag.SendTextMessageAsync(nachricht.Chat.Id, "Sorry, no Sub des Tages gefunden");
                            await Mittag.SendTextMessageAsync(BotNet, e.InnerException.Message);
                        }
                    }
                    #endregion
                }
                #region SetzeOg
                if (nachricht.ReplyToMessage?.MessageId == SetoKaibaMessageID)
                {
                    ReplyKeyboardHide rkh = new ReplyKeyboardHide();
                    rkh.HideKeyboard = true;
                    DateTime ogTime;
                    IsOgTime = ZeitStringZuDatum(nachricht.Text, out ogTime);

                    if (IsOgTime)
                    {
                        OgTime = ogTime;
                        await Mittag.SendTextMessageAsync(nachricht.Chat.Id, $"Alles klar, ogog um {OgTime.ToString("HH:mm")}",replyToMessageId: nachricht.MessageId, replyMarkup: rkh);
                    }
                    else if (nachricht.Text == "Kein ogog")
                    {
                        IsOgTime = false;
                        await Mittag.SendTextMessageAsync(nachricht.Chat.Id, "Okay, dann halt kein ogog", replyToMessageId: nachricht.MessageId, replyMarkup: rkh);
                    }
                    else
                        await Mittag.SendTextMessageAsync(nachricht.Chat.Id, $"Keine Uhrzeit gefunden. Bitte nur im Format HH:mm antworten. kthxbye;");

                }
                #endregion
                #region SetzeGeplant
                if (nachricht.ReplyToMessage?.MessageId == GeplantMessageID)
                {
                    ReplyKeyboardHide rkh = new ReplyKeyboardHide();
                    rkh.HideKeyboard = true;

                    IsGeplant = true;
                    Geplant = nachricht.Text;
                    Message gesendeteNachricht = await Mittag.SendTextMessageAsync(nachricht.Chat.Id, $"{Geplant} ist geplant", replyToMessageId: nachricht.MessageId, replyMarkup: rkh);

                }
                #endregion

            }
            catch (Exception exp)
            {
                SendException(exp);
            }

        }

        private void SendException(Exception e)
        {
            TelegramBotClient ExceptionBot = new TelegramBotClient("216102298:AAGO3Uy8jJRof7QunP-cjrYSRd4h4TEh7iM");

            ExceptionBot.SendTextMessageAsync(BotNet, e.InnerException.Message);
        }

        private async Task<string> getSubDesTages()
        {
            string subDesTages = @"http://subdestag.es/";

            HtmlWeb website = new HtmlWeb();
            HtmlDocument codeDesTages = await website.LoadFromWebAsync(subDesTages);
            HtmlNode hno = codeDesTages.DocumentNode.Descendants("h1").FirstOrDefault();   

            return hno.InnerText;
        }

        private bool ZeitStringZuDatum(string uhrzeit, out DateTime Zeit)
        {
            bool istZeit = false;
            Zeit = DateTime.Today;
            Regex reg = new Regex(@"(\d{2}):(\d{2})");

            if (reg.IsMatch(uhrzeit))
            {
                MatchCollection gefundeneZeit = reg.Matches(uhrzeit);
                double stunde, minute = 0;
                bool istDouble = Double.TryParse(gefundeneZeit[0].Groups[1].Value, out stunde);
                istDouble = istDouble && Double.TryParse(gefundeneZeit[0].Groups[2].Value, out minute);
                if (istDouble)
                {
                    Zeit = Zeit.AddHours(stunde).AddMinutes(minute);
                    istZeit = true;
                }
            }

            return istZeit;

        }

    }
}
