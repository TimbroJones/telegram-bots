﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot;

namespace RasPiBots
{
    class HurenBot
    {
        public TelegramBotClient Hure { get; set; }

        public HurenBot(TelegramBotClient tbc)
        {
            Hure = tbc;

            Hure.OnInlineQuery += Tbc_OnInlineQuery;
            Hure.OnInlineResultChosen += Tbc_OnInlineResultChosen;
            Hure.StartReceiving();
        }

        private void Tbc_OnInlineResultChosen(object sender, Telegram.Bot.Args.ChosenInlineResultEventArgs e)
        {
            string towelieHandtuch = "ed19c07d-bc46-4280-8c21-e8df7ab38d0d";
            TelegramBotClient Handtuch = new TelegramBotClient("240308319:AAEfGMAg0WFZEGvzd7lA3w4SSAjOGsd7kMs");

            if (e.ChosenInlineResult.ResultId == towelieHandtuch)
            {
                Handtuch.SendTextMessageAsync(e.ChosenInlineResult.From.Id, "Selber Handtuch!");
                Handtuch.SendTextMessageAsync(-78909426, "Selber Handtuch!");
            }

        }

        private void Tbc_OnInlineQuery(object sender, Telegram.Bot.Args.InlineQueryEventArgs args)
        {
            string towelieHandtuch = "ed19c07d-bc46-4280-8c21-e8df7ab38d0d";

            string query = args.InlineQuery.Query;
            string k = "";
            string e = "";

            if (query.StartsWith("!"))
            {
                k = "k";
                query = query.Substring(1);
            }
            if (query.StartsWith("e "))
            {
                e = "e";
                query = query.Substring(1);
            }

            query = query.Trim();

            string ein = $"{k}ein{e}";
            

            List<string> namen = new List<string>
                {
                    "Jonas",
                    "Köhler",
                    "Juan",
                    "Timbo",
                    "Nicole",
                    "Torben Bruhns"
                };

            if (query == "Handtuch")
                namen.Add("Towelie");

            TelegramBotClient HurenBot = (TelegramBotClient)sender;
            Telegram.Bot.Types.InlineQueryResults.InlineQueryResult[] einHurensohn = new Telegram.Bot.Types.InlineQueryResults.InlineQueryResult[namen.Count];


            for (int i = 0; i < namen.Count; i++)
            {
                string jonasIst = $"{namen[i]} ist {ein} {query}!";
                einHurensohn[i] = new Telegram.Bot.Types.InlineQueryResults.InlineQueryResultArticle();
                einHurensohn[i].Title = jonasIst;
                einHurensohn[i].Id = (namen[i] == "Towelie") ? towelieHandtuch : Guid.NewGuid().ToString();
                einHurensohn[i].InputMessageContent = new Telegram.Bot.Types.InputMessageContents.InputTextMessageContent() { MessageText = jonasIst };
            }
            
            HurenBot.AnswerInlineQueryAsync(args.InlineQuery.Id, einHurensohn);
        }
    }
}
